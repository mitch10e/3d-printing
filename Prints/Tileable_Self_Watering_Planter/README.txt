                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


http://www.thingiverse.com/thing:3175416
Tileable Self Watering Planter by Yamill is licensed under the Creative Commons - Attribution - Non-Commercial - Share Alike license.
http://creativecommons.org/licenses/by-nc-sa/3.0/

# Summary

UPDATE: Thanks to [DroneMang](https://www.thingiverse.com/DroneMang) who fixed some issues with the models slicing in Simplify3D. I've added his versions to the downloads (xxx_repaired). 
P.S. I haven't made a print with these models yet, so ask questions in the comments if something's up.

This is a tile-able self watering planter. It has a slot on the side where you can attach additional planters

# Print Settings

Printer Brand: Anycubic
Printer: All-metal Mega
Rafts: Doesn't Matter
Supports: No
Resolution: 0.2mm
Infill: 20%

# Post-Printing

## Sealing

Depending on your printer or printer settings, your mileage may vary on whether the watering cup is fully water-sealed. I recommend a layer or two of clear lacquer to close up any gaps.