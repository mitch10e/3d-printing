                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


http://www.thingiverse.com/thing:3195402
Ender 3 Enclosure (IKEA Lack Table) by Swiers_3D is licensed under the Creative Commons - Attribution - Non-Commercial license.
http://creativecommons.org/licenses/by-nc/3.0/

# Summary

This is a remake off the original Prusa i3 enclosure, made so a Creality Ender 3 can fit in it, with or without the printable shockdampeners.


If there are any questions do not hesitate to contact me ;)

Yoram Swiers  


# Print Settings

Printer: Ender 3
Rafts: No
Supports: Yes
Resolution: 0.2-0.3
Infill: 25%
Filament_brand: Colorfabb PLA
Filament_color: Grey
Filament_material: PLA

# What do you need

Everything what you need to build this enclosure is listed here.

- 4x  2x10x20mm 
https://www.ebay.com/itm/N50-10-100Pcs-Neodymium-Block-Magnet-20x10x2mm-Super-Strong-Rare-Earth-Magnets/291626809168?ssPageName=STRK%3AMEBIDX%3AIT&var=590660171980&_trksid=p2057872.m2749.l2649

- 3x 448x480mm and 2x 223x480mm plexiglass, in Belgium whereI live, I got my plexiglass from the following webshop.
https://www.plexikopen.be/plexiglas-helder-3-mm

- 12x M5x40mm 18x M4x20mm wood screws, you can get these at you local DIY shop.

This is everything you probably need to buy, other than that you will need some tools like a Dremel, double sided tape and some common sense and you'll be fine.

# Updates

- 8/11/2018 - added a cutting pattern for the 2 door pannels :)
- 11/11/2018 - added extra imperial files so people from the US can use 3.175mm plexiglass
- 09/01/2019 - redesigned the hinges so the plexi doesn't have to be glued in an the doors can be taken out. Recently broke my own hinges and realised that the design had some flaws...
- 22/01/2019 - All your prayers have been answered, edited the hinge system. Now you can normally pop the doors in and out without trashing the corners. ;) Will be tested soon ( ran out of filament.). Also added some leds and buttons to power on the printer. If needed  I will upload an electric scheme (it's really straighforward though, normally you should be able to install some led's without help ;) )